// ------------------ Fill the following details -----------------------------
// Student name: Nifin Noushad
// Student email:Nnoushad6085@conestogac.on.ca

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const { json } = require('body-parser');
const { check, validationResult } = require('express-validator');
const mongoose = require('mongoose');

//Regex for Customer Number and Name
var cNumberRegex = /^[1-9][0-9][\-][A-Z]{2}[\-][0-9]{3}$/;
var cNameRegex = /^[a-zA-Z\s]+$/;

//function for regex validation
function checkRegex(userInput, regex) {
    if (regex.test(userInput)) {
        return true;
    }
    return false;
}

//function for customer Name validation
function customerNameValidation(value) {
    if (!checkRegex(value, cNameRegex)) {
        throw new Error('Enter characters for Name field.');
    }
    return true;
}

//function for customer Number validation
function customerNumberValidation(value) {
    if (!checkRegex(value, cNumberRegex)) {
        throw new Error('Please enter the Customer Number in 12-BC-345. format.');
    }
    return true;
}

function productsValidation(value, { req }) {
    var blizzards = parseInt(req.body.blizzards);
    var shakes = parseInt(req.body.shakes);
    var sundaes = parseInt(req.body.sundaes);
    if (Math.sign(blizzards) != 1 && Math.sign(shakes) != 1 && Math.sign(sundaes) != 1) {
        throw new Error('Please enter positive integers for orders.');
    }
    return true;
}
mongoose.connect('mongodb://localhost:27017/final8020set3', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const Order = mongoose.model('Order', {
    customerName: String,
    customerNumber: String,
    blizzards: Number,
    sundaes: Number,
    shakes: Number
});

const User = mongoose.model('User', {
    userLogin: String,
    userPass: String
});

var myApp = express();
myApp.use(express.json());
myApp.use(session({
    secret: 'superrandomsecret',
    resave: false,
    saveUninitialized: true
}));

myApp.use(bodyParser.urlencoded({ extended: false }));

myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');

//------------- Use this space only for your routes ---------------------------

//Main Page
myApp.get('/', function (req, res) {
    res.render('home');
});

myApp.get('/home', function (req, res) {
    res.render('home');
});

myApp.get('/login', function (req, res) {
    res.render('login');
});

myApp.get('/placeorder', function (req, res) {
    res.render('placeorder');
});

myApp.get('/ordersucess', function (req, res) {
    res.render('ordersucess');
});


myApp.get('/contact', function (req, res) {
    res.render('contact');
});

myApp.post('/login', function (req, res) {
    var user = req.body.username;
    var pass = req.body.password;

    User.findOne({ userLogin: user, userPass: pass }).exec(function (err, admin) {
        // log any errors
        console.log('Error: ' + err);
        console.log('Admin: ' + admin);
        if (admin) {
            //store username in session and set logged in true
            req.session.username = user.userLogin;
            req.session.userLoggedIn = true;
            // redirect to the dashboard
            res.redirect('/orders');
        }
        else {
            res.render('login', { error: 'Sorry, cannot login!' });
        }
    });
});


myApp.get('/orders', function (req, res) {
    if (req.session.userLoggedIn) {
    Order.find({}).exec(function (err, orders) {
        console.log(err);
        res.render('orders', { orders: orders });
    });
}
else{
    res.redirect('/login');
}
});

//Add order
myApp.post('/process', [
    check('customerName', '').custom(customerNameValidation),
    check('customerNumber', '').custom(customerNumberValidation),
    check('blizzards' || 'sundaes' || 'shakes', '').custom(productsValidation),
], function (req, res) {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
        res.render('placeorder', { errors: errors.array() });
    }
    else {

        var customerName = req.body.customerName;
        var customerNumber = req.body.customerNumber;
        var blizzards = req.body.blizzards;
        var sundaes = req.body.sundaes;
        var shakes = req.body.shakes;

        // create an object with the fetched data to send to the view
        var addData = {
            customerName: customerName,
            customerNumber: customerNumber,
            blizzards: blizzards,
            sundaes: sundaes,
            shakes: shakes
        }

        // save data to database
        var newOrder = new Order(addData); // not correct yet, we need to fix it.
        newOrder.save();
            res.render('ordersucess', {addData:addData});
    }
});

//Delete page
//Use uniques mongodb id
myApp.get('/delete/:orderId', function (req, res) {
    // check if the user is logged in
    if (req.session.userLoggedIn) {
        //delete
        var orderId = req.params.orderId;
        console.log(orderId);
        Order.findByIdAndDelete({ _id: orderId }).exec(function (err, order) {
            console.log('Error: ' + err);
            console.log('Order: ' + order);
            if (order) {
                res.render('delete', { message: 'Order Successfully deleted!', userLoggedIn: req.session.userLoggedIn });
            }
            else {
                res.render('delete', { message: 'Sorry, could not delete!', userLoggedIn: req.session.userLoggedIn });
            }
        });
    }
    else {
        res.redirect('/login');
    }
});

//Logout Page
myApp.get('/logout', function (req, res) {
    //Remove variables from session
    req.session.username = '';
    req.session.userLoggedIn = false;
    //res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('logout', { message: "Thank you for using this application!. You are successfully logged out" });
});



//---------- Do not modify anything below this other than the port ------------
//------------------------ Setup the database ---------------------------------

myApp.get('/setup', function (req, res) {

    let userData = [{
        'userLogin': 'admin',
        'userPass': 'admin'
    }];

    User.collection.insertMany(userData);

    var firstNames = ['John ', 'Alana ', 'Jane ', 'Will ', 'Tom ', 'Leon ', 'Jack ', 'Kris ', 'Lenny ', 'Lucas '];
    var lastNames = ['May', 'Riley', 'Rees', 'Smith', 'Walker', 'Allen', 'Hill', 'Byrne', 'Murray', 'Perry'];

    let ordersData = [];

    for (i = 0; i < 10; i++) {
        let tempMemb = Math.floor((Math.random() * 100)) + '-AB' + '-' + Math.floor((Math.random() * 1000))
        let tempName = firstNames[Math.floor((Math.random() * 10))] + lastNames[Math.floor((Math.random() * 10))];
        let tempOrder = {
            customerName: tempName,
            customerNumber: tempMemb,
            blizzards: Math.floor((Math.random() * 10)),
            sundaes: Math.floor((Math.random() * 10)),
            shakes: Math.floor((Math.random() * 10))
        };
        ordersData.push(tempOrder);
    }

    Order.collection.insertMany(ordersData);
    res.send('Database setup complete. You can now proceed with your exam.');

});

//----------- Start the server -------------------

myApp.listen(8080);// change the port only if 8080 is blocked on your system
console.log('Server started at 8080 for mywebsite...');