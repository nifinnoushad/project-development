# How to install node.js and Run the Project

1. You should have install node.js environment in you machine
2. Go to the link "https://nodejs.org/en/download/" and follow the instruction to install the node.js environment.
3. Open the project.
4. Open the terminal for the project.
5. Type "node index.js "
6. You can see the success message in the terminal and the PORT which is it using.
7. Take any browser and type open the link using the PORT eg : localhost://8080.
8. You can see the site is running.
9. If you are seeing any errors in the terminal troubleshoot them and find the solution for it.
10. If you are making any changes in the code stop start the server and refresh the browser page to effect the change.

# License Details

GNU AGPLv3:

Permissions of this strongest copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. When a modified version is used to provide a service over a network, the complete source code of the modified version must be made available.